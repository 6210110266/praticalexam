const express = require('express')
const bodyParser = require('body-parser')
const cors = require('cors')
const app = express()
const port = 8080

num = 0

app.use(bodyParser.urlencoded({extended: false}))
app.use(bodyParser.json())


app.use(cors())

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.post('/api/increase', bodyParser.json(),async  (req, res) => {
    let num = parseInt(req.body.num)
    num = 0
    res.send(''+ (num+1))
    console.log(num)
})

app.get('/api/refresh', bodyParser.json(),async (req, res) => {
    let num = parseInt(req.body.num)
    num = 0
    res.send(''+ (num))
    console.log(num)
})


app.listen(port, () => {
  console.log(`Server listening on port ${port}`)
})
