import React, { useState } from 'react'
const axios = require('axios')

function App() {

  const [data, setData] = useState('')

  const onIncrease = async () => {
    let result = await axios.post('http://localhost:8080/api/increase')
    console.log(result.data)
    setData(result.data)

  }

  const onRefresh = async () => {
    let result = await axios.get('http://localhost:8080/api/refresh')
    console.log(result.data)
    setData(result.data)
  }

  return (
    <div>
      <div>{data}</div>
      <button onClick={onIncrease}>Increase</button>
      <button onClick={onRefresh}>Refresh</button>
    </div>
  );
}

export default App;
